import functools
import typing

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Model


class SearchString(str):

    def get_contains_query(self, filed: str) -> models.Q:
        return models.Q(**{f'{filed}__contains': self})


class SearchService:
    queryset: models.QuerySet
    search: SearchString
    fields: typing.Tuple[str]

    def __init__(self, queryset: models.QuerySet, search: SearchString, fields: typing.Tuple[str]) -> None:
        self.queryset = queryset
        self.search = search
        self.fields = fields

    def get_contains_queries(self) -> typing.Generator[models.Q, None, None]:
        return (
            self.search.get_contains_query(field)
            for field
            in self.fields
        )

    @staticmethod
    def join_queries_by_or(q1: models.Q, q2: models.Q) -> models.Q:
        return q1 | q2

    def get_reduced_queries(self) -> models.Q:
        return functools.reduce(
            self.join_queries_by_or,
            self.get_contains_queries(),
            models.Q()
        )

    def get_search_queryset(self) -> models.QuerySet:
        return self.queryset.filter(self.get_reduced_queries())


ModelType = typing.TypeVar('ModelType', bound=Model)


class AuthorityObjectPermission:
    authority_field_name: str
    obj: ModelType

    def __init__(self, obj: ModelType, authority_field_name: str):
        self.obj = obj
        self.authority_field_name = authority_field_name

    def get_object_owner(self) -> typing.Optional[AbstractUser]:
        return getattr(self.obj, self.authority_field_name, None)

    def check(self, user: AbstractUser) -> bool:
        return self.get_object_owner() == user
