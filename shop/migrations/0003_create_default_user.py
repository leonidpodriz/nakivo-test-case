import os

from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.db import migrations


def create_default_user(apps, _):
    User: 'User' = get_user_model()

    User.objects.get_or_create(
        username=os.getenv("SUPERUSER_USERNAME") or 'superuser',
        email=os.getenv("SUPERUSER_EMAIL") or 'super@user.com',
        password=make_password(os.getenv("SUPERUSER_PASSWORD") or 'Test1234$'),
    )


class Migration(migrations.Migration):
    dependencies = [
        ('shop', '0002_auto_20210722_1057'),
    ]

    operations = [
        migrations.RunPython(create_default_user),
    ]
