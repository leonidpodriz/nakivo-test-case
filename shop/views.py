import typing

from django.db import models
from django.urls import reverse_lazy
from django.views.generic import DetailView

from common.views import AuthorityCreateView, AuthorityUpdateView, SearchListView
from shop.models import Product


class ProductsListView(SearchListView):
    queryset: models.QuerySet = Product.objects.all()
    search_fields: typing.Tuple[str] = ('name', 'description')


class ProductsDetailView(DetailView):
    queryset: models.QuerySet = Product.objects.all()


class ProductsUpdateView(AuthorityUpdateView):
    login_url: str = reverse_lazy('login')
    queryset: models.QuerySet = Product.objects.all()
    fields: typing.Tuple[str] = ('name', 'description', 'price')


class ProductsCreateView(AuthorityCreateView):
    login_url: str = reverse_lazy('login')
    queryset: models.QuerySet = Product.objects.all()
    template_name: str = 'shop/product_create.html'
    fields: typing.Tuple[str] = ('name', 'description', 'price')
